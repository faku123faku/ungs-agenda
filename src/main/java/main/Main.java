package main;

import java.io.IOException;

import javax.swing.JOptionPane;

import modelo.Agenda;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.Controlador;
import presentacion.vista.VentanaConfig;
import presentacion.vista.Vista;
import utils.Credentials;


public class Main 
{

	public static void main(String[] args) 
	{
		Credentials cred = new Credentials();
		if(!cred.isInitialized()) {
			VentanaConfig config;
			config= VentanaConfig.getInstance();
			config.mostrarVentana();
			config.getBtnGuardar().setVisible(false);
			config.getBtnConfirmar().setVisible(true);
			config.getBtnConfirmar().addActionListener(e->{
				try {
					cred.setMysqlUser(config.getTxtUsuario().getText());
					cred.setMysqlPassw(config.getTxtPassw().getText());
					cred.setMysqlServer(config.getTxtServer().getText());
					cred.saveChanges();
					config.getBtnConfirmar().setVisible(false);
					config.getBtnGuardar().setVisible(true);
					config.cerrar();
					
					Vista vista = new Vista();
					Agenda modelo = new Agenda(new DAOSQLFactory());
					Controlador controlador = new Controlador(vista, modelo);
					controlador.inicializar();
					cred.setStatus("1");
					cred.saveChanges();
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "Fallo en la configuracion inicial, por favor revise que las credenciales sean correctas.");
				}
								
			});
		}else {
			Vista vista = new Vista();
			Agenda modelo = new Agenda(new DAOSQLFactory());
			Controlador controlador = new Controlador(vista, modelo);
			controlador.inicializar();
		}

	}
}
