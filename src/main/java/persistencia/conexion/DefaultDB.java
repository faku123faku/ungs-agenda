package persistencia.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class DefaultDB {

	private final String dbName = "agenda_g6";
	private String usr;
	private String pwd;
	private String server;

	public DefaultDB(String usr, String pwd, String server) {
		this.usr = usr;
		this.pwd = pwd;
		this.server=server;
	}

	public void generateDefaultBD() {
		this.createDefaultDB();
		this.createDefaultTables();
	}
	
	private void createDefaultDB() {
		// Defines the JDBC URL. As you can see, we are not specifying
		// the database name in the URL.
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://"+server+"/";

			// Defines username and password to connect to database server.
			String username = usr;
			String password = pwd;

			// SQL command to create a database and tables in MySQL.
			String sql = "CREATE DATABASE IF NOT EXISTS "+dbName+";";
			Connection conn = DriverManager.getConnection(url, username, password);
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.execute();
			
			conn.close();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Fallo en la configuracion inicial, por favor revise que las credenciales sean correctas.");
			e.printStackTrace();
		}

	}

	private void createDefaultTables() {

		// Defines the JDBC URL. As you can see, we are not specifying
		// the database name in the URL.
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://"+server+"/"+dbName;

			String username = usr;
			String password = pwd;

			String sql = "CREATE TABLE IF NOT EXISTS `personas` (\r\n" + 
					"  `idPersona` int NOT NULL AUTO_INCREMENT,\r\n" + 
					"  `Nombre` varchar(45) NOT NULL,\r\n" + 
					"  `Telefono` varchar(20) NOT NULL,\r\n" + 
					"  `Calle` varchar(45) DEFAULT NULL,\r\n" + 
					"  `Altura` varchar(6) DEFAULT NULL,\r\n" + 
					"  `Piso` varchar(3) DEFAULT NULL,\r\n" + 
					"  `Dpto` varchar(6) DEFAULT NULL,\r\n" + 
					"  `Localidad` varchar(64) DEFAULT NULL,\r\n" + 
					"  `Email` varchar(64) DEFAULT NULL,\r\n" + 
					"  `Fecha_cumple` date DEFAULT NULL,\r\n" + 
					"  `Tipo_contacto` varchar(45) DEFAULT NULL,\r\n" + 
					"  `Favorito` tinyint NOT NULL DEFAULT '0' COMMENT 'Contacto Favorito es 1, para no favoritos es 0',\r\n" + 
					"  PRIMARY KEY (`idPersona`)\r\n" + 
					") ;";
			
			String sql2="CREATE TABLE IF NOT EXISTS `contactos` (\r\n" + 
					"  `idContacto` int NOT NULL AUTO_INCREMENT,\r\n" + 
					"  `tipoContacto` varchar(45) DEFAULT NULL,\r\n" + 
					"  PRIMARY KEY (`idContacto`)\r\n" + 
					");";
			
			Connection conn = DriverManager.getConnection(url, username, password);
			PreparedStatement stmt = conn.prepareStatement(sql);
			PreparedStatement stmt2 = conn.prepareStatement(sql2);
			stmt.execute();
			stmt2.execute();
			
			conn.close();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Fallo en la configuracion inicial, por favor revise que las credenciales sean correctas.");
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
