package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ContactoDAO;
import dto.ContactoDTO;
import dto.PersonaDTO;

public class ContactoDAOSQL implements ContactoDAO {
	
	private static final String insert = "INSERT INTO contactos(idContacto, tipoContacto) VALUES(?, ?)";
	private static final String delete = "DELETE FROM contactos WHERE idContacto = ?";
	private static final String readall = "SELECT * FROM contactos";
	private static final String leer = "SELECT tipoContacto FROM contactos";
	private static final String update = "UPDATE contactos SET tipoContacto=? WHERE idContacto = ?";
	
	public boolean insert(ContactoDTO contacto) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, contacto.getIdContacto());
			statement.setString(2, contacto.getTipoContacto());
						
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	public boolean delete(ContactoDTO contacto_a_eliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(contacto_a_eliminar.getIdContacto()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	
	
	public boolean update(ContactoDTO contacto) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try
		{
			statement = conexion.prepareStatement(update);
			
			statement.setString(1, contacto.getTipoContacto());
			statement.setInt(2, contacto.getIdContacto());
			
			
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isUpdateExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isUpdateExitoso;
	}
	
	public List<ContactoDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<ContactoDTO> contactos = new ArrayList<ContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				
				contactos.add(getContactoDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return contactos;
	}

	private ContactoDTO getContactoDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idContacto");
		String tipoContacto = resultSet.getString("tipoContacto");
		
		return new ContactoDTO(id, tipoContacto);
	}

	public List<String> leer() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<String> contactos = new ArrayList<String>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				
				contactos.add(getContacto(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return contactos;
	}

	private String getContacto(ResultSet resultSet) throws SQLException
	{
		//int id = resultSet.getInt("idContacto");
		String tipoContacto = resultSet.getString("tipoContacto");
		
		return tipoContacto;
	}
	
}
