package dto;

public class ContactoDTO {
	private int idContacto;
	private String tipoContacto;
	
	public ContactoDTO(int idContacto, String tipoContacto)
	{
		this.idContacto = idContacto;
		this.tipoContacto = tipoContacto;
	}
	
	public int getIdContacto() 
	{
		return this.idContacto;
	}

	public void setIdContacto(int idContacto) 
	{
		this.idContacto = idContacto;
	}

	public String getTipoContacto() 
	{
		return this.tipoContacto;
	}

	public void setTipoContacto(String tipoContacto) 
	{
		this.tipoContacto = tipoContacto;
	}
}
