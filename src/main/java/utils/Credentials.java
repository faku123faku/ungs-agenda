package utils;

import java.io.File;
import java.io.IOException;

import org.ini4j.Ini;

public class Credentials {

	private Ini credentials;
	
	public Credentials() {
		try {
			credentials = new Ini(new File ("config/credentials.ini"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getMysqlUser() {
		return credentials.get("MysqlCredentials", "username");
	}
	
	public void setMysqlUser(String newUser) {
		credentials.put("MysqlCredentials", "username", newUser);
	}
	
	public String getMysqlPassw() {
		return credentials.get("MysqlCredentials", "password");
	}
	
	public void setMysqlPassw(String newPassw) {
		credentials.put("MysqlCredentials", "password", newPassw);
	}
	
	public boolean isInitialized() {
		String status=credentials.get("Status", "initialized");
		if(status.equals("1")) {
			return true;
		}
		return false;
	}
	
	public void setStatus(String s) {
		credentials.put("Status", "initialized", s);
	}
	
	public String getMysqlServer() {
		return credentials.get("MysqlCredentials", "server");
	}
	
	public void setMysqlServer(String newServer) {
		credentials.put("MysqlCredentials", "server", newServer);
	}
	
	public void saveChanges() throws IOException {
		credentials.store();
	}
	
}
