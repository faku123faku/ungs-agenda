package utils;

import java.util.regex.Pattern;

public class FieldValidator {

	public FieldValidator() {
		
	}
	
	public boolean validateAltura(String altura) { 
		return Pattern.matches("^([0-9]{1,4})$", altura) || altura.isEmpty();
	}
	
	public boolean validatePiso(String piso) {
		return Pattern.matches("^[0-9]{1,3}$", piso) || piso.isEmpty();
	}
	
	public boolean validateDpto(String dpto) {
		return Pattern.matches("^[0-9]{1,3}$", dpto) || dpto.isEmpty() ;
	}
	
	public boolean validateDate(String date) {
		return Pattern.matches("^[0-9]{4}-(0[0-9]|1[0-2])-([0-2][0-9]|3[0-1])$", date) || date.isEmpty();
	}
	
	public boolean validateTelefono(String tel) {
		return Pattern.matches("^[0-9]*$", tel) || tel.isEmpty();
	}
	
	public boolean validateNombre(String nombre) {
		return !nombre.isEmpty();
	}
}
