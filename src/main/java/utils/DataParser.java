package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class DataParser {

	public DataParser() {
		
	}
	
	public ArrayList<String> getMunicipios (File j){
		ArrayList<String> municipios = new ArrayList<String>();
		JSONParser parser = new JSONParser();	
		FileReader r;
		try {	
			r = new FileReader(j.getAbsolutePath());
			Object obj = parser.parse(r);
			String json =obj.toString();
			JSONObject jsonOb = new JSONObject(json);
			JSONArray records = jsonOb.getJSONArray("municipios");
			
			for(Object elemento : records) {
				JSONObject datosMunicipio =(JSONObject) elemento;
				municipios.add(datosMunicipio.get("provincia_nombre")+" - "+datosMunicipio.get("nombre"));
				
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return municipios;
		
	}
	
	public String[] transformStringArray(List<String> lista) {
		String [] newList = new String[lista.size()];
		for(int i=0; i<lista.size(); i++) {
			newList[i]=lista.get(i);
		}
		return newList;
		
	}
	
	
}
