package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import modelo.Agenda;
import persistencia.conexion.DefaultDB;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaConfig;
import presentacion.vista.VentanaContacto;
import presentacion.vista.VentanaContactoEdit;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaPersonaEdit;
import presentacion.vista.Vista;
import utils.Credentials;
import utils.FieldValidator;
import dto.ContactoDTO;
import dto.PersonaDTO;

public class Controlador implements ActionListener {
	private Vista vista;
	private List<PersonaDTO> personasEnTabla;
	private List<ContactoDTO> contactosEnTabla;
	private VentanaPersona ventanaPersona;
	private VentanaPersonaEdit ventanaPersonaEdit;
	private Agenda agenda;
	private VentanaContacto ventanaContacto;
	private VentanaContactoEdit ventanaContactoEdit;
	private VentanaConfig ventanaConfig;
	private Credentials cred;

	public Controlador(Vista vista, Agenda agenda) {
		cred = new Credentials();

		this.ventanaConfig = VentanaConfig.getInstance();

		DefaultDB defaultdb = new DefaultDB(cred.getMysqlUser(), cred.getMysqlPassw(), cred.getMysqlServer());
		defaultdb.generateDefaultBD();

		this.vista = vista;
		this.vista.getBtnAgregar().addActionListener(a -> ventanaAgregarPersona(a));
		this.vista.getBtnBorrar().addActionListener(s -> borrarPersona(s));
		this.vista.getBtnBorrarContacto().addActionListener(s -> borrarContacto(s));
		this.vista.getBtnReporte().addActionListener(r -> mostrarReporte(r));
		this.vista.getBtnEditar().addActionListener(e -> ventanaEditarPersona(e));
		this.vista.getBtnAgregarContacto().addActionListener(c -> ventanaAgregarContacto(c));
		this.vista.getBtnConfig().addActionListener(c -> ventanaConfig(c));

		this.ventanaPersona = VentanaPersona.getInstance();
		this.ventanaContacto = VentanaContacto.getInstance();
		this.ventanaPersona.getBtnAgregarPersona().addActionListener(p -> guardarPersona(p));

		this.ventanaPersonaEdit = VentanaPersonaEdit.getInstance();
		this.ventanaPersonaEdit.getBtnEditarPersona().addActionListener(p -> editarPersona(p));

		this.vista.getBtnEditarContacto().addActionListener(c -> ventanaEditarContacto(c));

		this.ventanaContactoEdit = VentanaContactoEdit.getInstance();
		this.ventanaContactoEdit.getBtnEditarContacto().addActionListener(p -> editarContacto(p));

		this.ventanaContacto.getBtnAgregar().addActionListener(c -> guardarContacto(c));
		this.agenda = agenda;

		this.ventanaConfig.getBtnGuardar().addActionListener(e -> guardarCambios(e));

	}

	private void guardarCambios(ActionEvent e) {
		try {
			cred.setMysqlServer(ventanaConfig.getTxtServer().getText());
			cred.setMysqlUser(ventanaConfig.getTxtUsuario().getText());
			cred.setMysqlPassw(ventanaConfig.getTxtPassw().getText());
			cred.saveChanges();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ventanaConfig.cerrar();

	}

	private void ventanaConfig(ActionEvent c) {
		this.ventanaConfig.getTxtServer().setText(cred.getMysqlServer());
		this.ventanaConfig.getTxtUsuario().setText(cred.getMysqlUser());
		this.ventanaConfig.getTxtPassw().setText(cred.getMysqlPassw());
		this.ventanaConfig.mostrarVentana();
	}

	private void ventanaEditarPersona(ActionEvent a) {
		int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
		if (filasSeleccionadas.length > 0) {
			PersonaDTO p = this.agenda.obtenerPersonas().get(filasSeleccionadas[0]);
			this.ventanaPersonaEdit.setIdPersona(p.getIdPersona());
			this.ventanaPersonaEdit.getTxtNombre().setText(p.getNombre());
			this.ventanaPersonaEdit.getTxtTelefono().setText(p.getTelefono());
			this.ventanaPersonaEdit.getTxtCalle().setText(p.getCalle());
			this.ventanaPersonaEdit.getTxtAltura().setText(p.getAltura());
			this.ventanaPersonaEdit.getTxtPiso().setText(p.getPiso());
			this.ventanaPersonaEdit.getTxtDpto().setText(p.getDpto());
			this.ventanaPersonaEdit.getComboLocalidad().setSelectedItem(p.getLocalidad());
			this.ventanaPersonaEdit.getTxtCorreo().setText(p.getEmail());
			this.ventanaPersonaEdit.getComboTipoContacto().setSelectedItem(p.getTipoContacto());
			this.ventanaPersonaEdit.getTxtCumple().setText(p.getCumple());
			this.ventanaPersonaEdit.getChckbxFavoritos().setSelected(p.getFavorito() == 0 ? false : true);
			this.ventanaPersonaEdit.mostrarVentana();
		}

	}

	private void ventanaAgregarPersona(ActionEvent a) {
		this.ventanaPersona.mostrarVentana();
	}

	private void guardarPersona(ActionEvent p) { // Agregar reglas de validaci�n de formato
		String nombre = this.ventanaPersona.getTxtNombre().getText();
		String tel = ventanaPersona.getTxtTelefono().getText();
		String calle = ventanaPersona.getTxtCalle().getText();
		String altura = ventanaPersona.getTxtAltura().getText();
		String piso = ventanaPersona.getTxtPiso().getText();
		String dpto = ventanaPersona.getTxtDpto().getText();
		String localidad = ventanaPersona.getComboLocalidad().getSelectedItem().toString();
		String email = ventanaPersona.getTxtCorreo().getText();
		String cumple = ventanaPersona.getTxtCumple().getText();
		String tipoContacto = ventanaPersona.getComboTipoContacto().getSelectedItem().toString();

		int favorito = 0;
		if (ventanaPersona.getChckbxFavoritos().isSelected()) {
			favorito = 1;
		}

		if (camposCorrectos(nombre, tel, altura, piso, dpto, cumple, email)) {
			PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, tel, calle, altura, piso, dpto, localidad, email,
					cumple, tipoContacto, favorito);
			this.agenda.agregarPersona(nuevaPersona);
			this.refrescarTabla();
			this.ventanaPersona.cerrar();
		} else {
			JOptionPane.showMessageDialog(null, "Por favor, aseg�rese de que los campos contengan datos correctos");
		}

	}

	private void mostrarReporte(ActionEvent r) {
		ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonas());

		reporte.mostrar();
	}

	public void borrarPersona(ActionEvent s) {
		int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
		for (int fila : filasSeleccionadas) {
			this.agenda.borrarPersona(this.personasEnTabla.get(fila));
		}

		this.refrescarTabla();
	}

	private void editarPersona(ActionEvent e) {

		int idPersona = this.ventanaPersonaEdit.getIdPersona();
		String nombre = this.ventanaPersonaEdit.getTxtNombre().getText();
		String tel = ventanaPersonaEdit.getTxtTelefono().getText();
		String calle = ventanaPersonaEdit.getTxtCalle().getText();
		String altura = ventanaPersonaEdit.getTxtAltura().getText();
		String piso = ventanaPersonaEdit.getTxtPiso().getText();
		String dpto = ventanaPersonaEdit.getTxtDpto().getText();
		String localidad = ventanaPersonaEdit.getComboLocalidad().getSelectedItem().toString();
		String email = ventanaPersonaEdit.getTxtCorreo().getText();
		String cumple = ventanaPersonaEdit.getTxtCumple().getText();
		String tipoContacto = ventanaPersonaEdit.getComboTipoContacto().getSelectedItem().toString();

		int favorito = 0;
		if (ventanaPersonaEdit.getChckbxFavoritos().isSelected()) {
			favorito = 1;
		}

		if (camposCorrectos(nombre, tel, altura, piso, dpto, cumple, email)) {
			PersonaDTO nuevaPersona = new PersonaDTO(idPersona, nombre, tel, calle, altura, piso, dpto, localidad,
					email, cumple, tipoContacto, favorito);
			this.agenda.modificarPersona(nuevaPersona);
			this.refrescarTabla();
			this.ventanaPersonaEdit.cerrar();
		} else {
			JOptionPane.showMessageDialog(null, "Por favor, aseg�rese de que los campos contengan datos correctos");
		}

	}

	public void inicializar() {

		this.refrescarTabla();
		this.vista.show();

	}

	private void refrescarTabla() {
		this.personasEnTabla = agenda.obtenerPersonas();
		this.vista.llenarTabla(this.personasEnTabla);
		this.contactosEnTabla = agenda.obtenerContactos();
		this.vista.llenarTablaContactos(this.contactosEnTabla);
	}

	private boolean camposCorrectos(String nombre, String tel, String altura, String piso, String dpto, String cumple,
			String mail) {
		FieldValidator fv = new FieldValidator();
		if (fv.validateNombre(nombre) && fv.validateTelefono(tel) && fv.validateAltura(altura) && fv.validatePiso(piso)
				&& fv.validateDpto(dpto) && fv.validateDate(cumple)) {
			if (validarMail(mail)) {
				return true;
			}
		}
		return false;
	}

	private boolean validarMail(String mail) {
		String s = "([a-z0-9_]+@(hotmail||gmail).com)?";
		Pattern patron = Pattern.compile(s);
		Matcher m = patron.matcher(mail);
		if (m.matches()) {
			return true;
		}
		return false;
	}

	private void ventanaAgregarContacto(ActionEvent a) {
		this.ventanaContacto.mostrarVentana();
	}

	private void guardarContacto(ActionEvent p) { // Agregar reglas de validaci�n de formato
		String tipoContacto = this.ventanaContacto.getTxtContacto().getText();

		if (!tipoContacto.isEmpty()) {
			ContactoDTO nuevaPersona = new ContactoDTO(0, tipoContacto);
			this.agenda.agregarContacto(nuevaPersona);
			this.refrescarTabla();
			this.ventanaContacto.cerrar();
		} else {
			JOptionPane.showMessageDialog(null, "Por favor, aseg�rese de que los campos contengan datos correctos");
		}

	}

	private void ventanaEditarContacto(ActionEvent a) {
		int[] filasSeleccionadas = this.vista.getTablaContactos().getSelectedRows();
		if (filasSeleccionadas.length > 0) {
			ContactoDTO p = this.agenda.obtenerContactos().get(filasSeleccionadas[0]);
			this.ventanaContactoEdit.setIdContacto(p.getIdContacto());
			this.ventanaContactoEdit.getTxtContacto().setText(p.getTipoContacto());
			this.ventanaContactoEdit.mostrarVentana();
		}

	}

	private void editarContacto(ActionEvent e) {

		int idContacto = this.ventanaContactoEdit.getIdContacto();
		String tipoContacto = this.ventanaContactoEdit.getTxtContacto().getText();

		if (!tipoContacto.isEmpty()) {
			ContactoDTO nuevoContacto = new ContactoDTO(idContacto, tipoContacto);
			this.agenda.modificarContacto(nuevoContacto);
			this.refrescarTabla();
			this.ventanaContactoEdit.cerrar();
		} else {
			JOptionPane.showMessageDialog(null, "Por favor, aseg�rese de que los campos contengan datos correctos");
		}

	}

	public void borrarContacto(ActionEvent s) {
		int[] filasSeleccionadas = this.vista.getTablaContactos().getSelectedRows();
		for (int fila : filasSeleccionadas) {
			this.agenda.borrarContacto(this.contactosEnTabla.get(fila));
		}

		this.refrescarTabla();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
