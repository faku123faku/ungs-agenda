package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VentanaContactoEdit extends JFrame{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static VentanaContactoEdit INSTANCE;
	private JButton btnEditarContacto;
	private JLabel lblContacto;
	private JTextField txtContacto;
	private int idContacto;

	
	public static VentanaContactoEdit getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaContactoEdit(); 	
			return new VentanaContactoEdit();
		}
		else
			return INSTANCE;
	}
	
	private VentanaContactoEdit() 
	{
		super();
		setTitle("Agregar nuevo tipo de contacto");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 400, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 400, 200);
		contentPane.add(panel);
		panel.setLayout(null);
		
		lblContacto = new JLabel("Tipo contacto");
		lblContacto.setBounds(10, 10, 132, 14);
		panel.add(lblContacto);
		
		txtContacto = new JTextField();
		txtContacto.setColumns(10);
		txtContacto.setBounds(100, 10, 164, 20);
		panel.add(txtContacto);
		
		btnEditarContacto = new JButton("Agregar");
		btnEditarContacto.setBounds(10, 100, 89, 23);
		panel.add(btnEditarContacto);
		
		this.setVisible(false);
	}


	public void mostrarVentana()
	{
		this.setVisible(true);
	}

	public JTextField getTxtContacto() {
		return txtContacto;
	}

	public JButton getBtnEditarContacto() 
	{
		return btnEditarContacto;
	}

	public void cerrar()
	{
		this.txtContacto.setText(null);
		this.dispose();
	}

	public void setIdContacto(int id) {
		this.idContacto=id;
	}

	public int getIdContacto() {
		return this.idContacto;
	}
}
