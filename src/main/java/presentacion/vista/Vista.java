package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dto.ContactoDTO;
import dto.PersonaDTO;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import persistencia.conexion.Conexion;

public class Vista
{
	private JFrame frmAgendaG;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnBorrar;
	private JButton btnEditar;
	private JButton btnReporte;
	private DefaultTableModel modelPersonas;
	private  String[] nombreColumnas = {"Nombre y apellido","Telefono","Calle","Altura","Piso","Dpto","Localidad","Email","Cumplea�o","Tipo","Favorito"};

	private JTable tablaContactos;
	private DefaultTableModel modelContactos;
	private  String[] nombreColumnasContacto = {"Id contacto","Tipo de Contacto"};
	private JButton btnAgregarContacto;
	private JButton btnBorrarContacto;
	private JButton btnEditarContacto;
	private JButton btnConfig;
	
	public Vista() 
	{
		super();
		initialize();
	}


	private void initialize() 
	{
		frmAgendaG = new JFrame();
		frmAgendaG.setTitle("Agenda G6");
		frmAgendaG.setBounds(100, 100, 900, 300);//700
		frmAgendaG.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAgendaG.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 900, 262);//674
		frmAgendaG.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(10, 11, 654, 182);
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);
		
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		spPersonas.setViewportView(tablaPersonas);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(10, 228, 89, 23);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(109, 228, 89, 23);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(208, 228, 89, 23);
		panel.add(btnBorrar);
		
		btnReporte = new JButton("Reporte");
		btnReporte.setBounds(307, 228, 89, 23);
		panel.add(btnReporte);
		
		JScrollPane spContactos = new JScrollPane();
		spContactos.setBounds(700, 11, 150, 182);//654
		panel.add(spContactos);
		
		modelContactos = new DefaultTableModel(null,nombreColumnasContacto);
		tablaContactos = new JTable(modelContactos);
		
		tablaContactos.getColumnModel().getColumn(0).setPreferredWidth(100);
		tablaContactos.getColumnModel().getColumn(0).setResizable(false);
		tablaContactos.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaContactos.getColumnModel().getColumn(1).setResizable(false);
		spContactos.setViewportView(tablaContactos);
		
		btnAgregarContacto = new JButton("Agregar");
		btnAgregarContacto.setBounds(580, 228, 89, 23);
		panel.add(btnAgregarContacto);
		
		btnEditarContacto = new JButton("Editar");
		btnEditarContacto.setBounds(680, 228, 89, 23);
		panel.add(btnEditarContacto);
		
		btnBorrarContacto = new JButton("Borrar");
		btnBorrarContacto.setBounds(780, 228, 89, 23);
		panel.add(btnBorrarContacto);
		
		btnConfig = new JButton("Configuracion");
		btnConfig.setBounds(406, 228, 117, 23);
		panel.add(btnConfig);
	}

	public void show()
	{
		this.frmAgendaG.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frmAgendaG.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "¿Estás seguro que quieres salir de la Agenda?", 
		             "Confirmación", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frmAgendaG.setVisible(true);
	}
	
	public JButton getBtnConfig() {
		return btnConfig;
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnEditar() {
		return btnEditar;
	}
	
	public JButton getBtnAgregarContacto() 
	{
		return btnAgregarContacto;
	}

	public JButton getBtnBorrarContacto() 
	{
		return btnBorrarContacto;
	}
	
	public JButton getBtnEditarContacto() {
		return btnEditarContacto;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}
	
	public DefaultTableModel getModelContactos() 
	{
		return modelContactos;
	}
	
	public JTable getTablaContactos()
	{
		return tablaContactos;
	}

	public String[] getNombreColumnasContacto() 
	{
		return nombreColumnasContacto;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}

	public void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());
		for (PersonaDTO p : personasEnTabla)
		{
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			String calle = p.getCalle();
			String altura = p.getAltura();
			String piso = p.getPiso();
			String dpto = p.getDpto();
			String localidad = p.getLocalidad();
			String email = p.getEmail();
			String cumple = p.getCumple();
			String tipoContacto = p.getTipoContacto();
			String favorito =p.getFavorito()==1?"Si":"No";
			
			Object[] fila = {nombre, tel, calle, altura, piso, dpto, localidad, email, cumple, tipoContacto, favorito};
			this.getModelPersonas().addRow(fila);

		}
		
	}
	
	public void llenarTablaContactos(List<ContactoDTO> contactosEnTabla) {
		this.getModelContactos().setRowCount(0); //Para vaciar la tabla
		this.getModelContactos().setColumnCount(0);
		this.getModelContactos().setColumnIdentifiers(this.getNombreColumnasContacto());
		for (ContactoDTO c :contactosEnTabla)
		{
			int id = c.getIdContacto();
			String tipoContacto = c.getTipoContacto();
			
			Object[] fila = {id, tipoContacto};
			this.getModelContactos().addRow(fila);
		}	
	}
}
