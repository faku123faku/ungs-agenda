package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import presentacion.vista.AutoCompletion;
import persistencia.dao.mysql.ContactoDAOSQL;
import utils.DataParser;

import javax.swing.JSeparator;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;

import java.io.File;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JSpinner;

import java.awt.Color;

//import javax.swing.DefaultComboBoxModel;
//import javax.swing.JComboBox;
//import javax.swing.JFrame;
//import javax.swing.JTextField;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;


public class VentanaPersonaEdit extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JButton btnEditarPersona;
	private static VentanaPersonaEdit INSTANCE;
	private JLabel lblCalle;
	private JLabel lblAltura;
	private JLabel lblPiso;
	private JLabel lblDpto;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtPiso;
	private JTextField txtDpto;
	private JLabel lblLocalidad;
	private JLabel lblDomicilio;
	private JSeparator separator_datosPersonales;
	private JLabel lblDatosPersonales;
	private JSeparator separator_Contacto;
	private JLabel lblContacto;
	private JLabel lblCumple;
	private JTextField txtCumple;
	private JTextField txtCorreo;
	private JLabel lblCorreoElectrnico;
	private JSeparator separator;
	private JLabel lblEspecial;
	private JLabel lblTipoDeContacto;
	private JComboBox comboLocalidad;
	//private FilterComboBox comboLocalidad;
	private JComboBox comboTipoContacto;
	private JCheckBox chckbxFavoritos;
	private int idPersona;
	private JLabel lblFavoritos;
	private DataParser municipios;
	
	private final String pathMunicipioFile = "json/municipios.json";
	private JLabel lblaaaammdd;
	
	public static VentanaPersonaEdit getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaPersonaEdit(); 	
			return new VentanaPersonaEdit();
		}
		else
			return INSTANCE;
	}

	private VentanaPersonaEdit() 
	{
		super();
		setTitle("Editar Contacto");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 343, 522);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 307, 462);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido(*)");
		lblNombreYApellido.setBounds(10, 38, 130, 14);
		panel.add(lblNombreYApellido);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 276, 113, 14);
		panel.add(lblTelfono);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(140, 35, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(133, 273, 164, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		btnEditarPersona = new JButton("Guardar");
		btnEditarPersona.setBounds(208, 428, 89, 23);
		panel.add(btnEditarPersona);
		
		lblCalle = new JLabel("Calle");
		lblCalle.setBounds(10, 155, 113, 14);
		panel.add(lblCalle);
		
		lblAltura = new JLabel("Altura");
		lblAltura.setBounds(10, 183, 48, 14);
		panel.add(lblAltura);
		
		lblPiso = new JLabel("Piso");
		lblPiso.setBounds(128, 183, 37, 14);
		panel.add(lblPiso);
		
		lblDpto = new JLabel("Dpto");
		lblDpto.setBounds(213, 183, 37, 14);
		panel.add(lblDpto);
		
		txtCalle = new JTextField();
		txtCalle.setColumns(10);
		txtCalle.setBounds(133, 152, 164, 20);
		panel.add(txtCalle);
		
		txtAltura = new JTextField();
		txtAltura.setColumns(10);
		txtAltura.setBounds(56, 180, 48, 20);
		panel.add(txtAltura);
		
		txtPiso = new JTextField();
		txtPiso.setColumns(10);
		txtPiso.setBounds(161, 180, 37, 20);
		panel.add(txtPiso);
		
		txtDpto = new JTextField();
		txtDpto.setColumns(10);
		txtDpto.setBounds(260, 180, 37, 20);
		panel.add(txtDpto);
		
		lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(10, 211, 113, 14);
		panel.add(lblLocalidad);
		
		JSeparator separator_Domicilio = new JSeparator();
		separator_Domicilio.setBounds(10, 137, 287, 7);
		panel.add(separator_Domicilio);
		
		lblDomicilio = new JLabel("Domicilio");
		lblDomicilio.setBounds(10, 123, 122, 14);
		panel.add(lblDomicilio);
		
		separator_datosPersonales = new JSeparator();
		separator_datosPersonales.setBounds(10, 25, 287, 2);
		panel.add(separator_datosPersonales);
		
		lblDatosPersonales = new JLabel("Datos Personales");
		lblDatosPersonales.setBounds(10, 11, 122, 14);
		panel.add(lblDatosPersonales);
		
		separator_Contacto = new JSeparator();
		separator_Contacto.setBounds(10, 260, 287, 2);
		panel.add(separator_Contacto);
		
		lblContacto = new JLabel("Contacto");
		lblContacto.setBounds(10, 246, 122, 14);
		panel.add(lblContacto);
		
		lblCumple = new JLabel("Fecha de Cumplea\u00F1os");
		lblCumple.setBounds(10, 78, 122, 14);
		panel.add(lblCumple);
		
		txtCumple = new JTextField();
		txtCumple.setColumns(10);
		txtCumple.setBounds(143, 75, 154, 20);
		panel.add(txtCumple);
		
		txtCorreo = new JTextField();
		txtCorreo.setColumns(10);
		txtCorreo.setBounds(133, 304, 164, 20);
		panel.add(txtCorreo);
		
		lblCorreoElectrnico = new JLabel("Email");
		lblCorreoElectrnico.setBounds(10, 307, 94, 14);
		panel.add(lblCorreoElectrnico);
		
		separator = new JSeparator();
		separator.setBounds(10, 357, 287, 2);
		panel.add(separator);
		
		lblEspecial = new JLabel("Especial");
		lblEspecial.setBounds(12, 342, 122, 14);
		panel.add(lblEspecial);
		
		lblTipoDeContacto = new JLabel("Tipo de Contacto");
		lblTipoDeContacto.setBounds(12, 366, 122, 14);
		panel.add(lblTipoDeContacto);
		
		comboTipoContacto = new JComboBox();
		ContactoDAOSQL contacto= new ContactoDAOSQL();
		ArrayList<String> list=(ArrayList<String>) contacto.leer();
		comboTipoContacto.setModel(new DefaultComboBoxModel(list.toArray()));
		comboTipoContacto.setBounds(208, 363, 89, 20);
		
		panel.add(comboTipoContacto);
		
		municipios = new DataParser();	
		//FilterComboBox comboLocalidad = new FilterComboBox(municipios.getMunicipios(new File(this.pathMunicipioFile)));
		comboLocalidad = new JComboBox();
		comboLocalidad.setModel(new DefaultComboBoxModel(municipios.transformStringArray(municipios.getMunicipios(new File(this.pathMunicipioFile)))));
		comboLocalidad.setBounds(133, 208, 164, 20);
		enable(comboLocalidad);
		panel.add(comboLocalidad);
		
		lblFavoritos = new JLabel("A\u00F1adir a Favoritos?");
		lblFavoritos.setBounds(50, 402, 120, 14);
		panel.add(lblFavoritos);
		
		JLabel lblEstrella = new JLabel("★");
		lblEstrella.setForeground(Color.YELLOW);
		lblEstrella.setFont(new java.awt.Font("NanumGothic",0,36));
		lblEstrella.setBounds(10, 390, 122, 30);
		panel.add(lblEstrella);
		
		chckbxFavoritos = new JCheckBox("");
		chckbxFavoritos.setBounds(170, 398, 21, 23);
		panel.add(chckbxFavoritos);
		
		lblaaaammdd = new JLabel("*(AAAA-MM-DD)");
		lblaaaammdd.setBounds(177, 99, 89, 14);
		panel.add(lblaaaammdd);
		
		JLabel lblComentario = new JLabel("(*)Campos obligatorios");
		lblComentario.setBounds(10, 440, 140, 14);
		panel.add(lblComentario);
		
		this.setVisible(false);
	}
	
	public JTextField getTxtCalle() {
		return txtCalle;
	}

	public JTextField getTxtAltura() {
		return txtAltura;
	}

	public JTextField getTxtPiso() {
		return txtPiso;
	}

	public JTextField getTxtDpto() {
		return txtDpto;
	}

	public JTextField getTxtCumple() {
		return txtCumple;
	}

	public JTextField getTxtCorreo() {
		return txtCorreo;
	}

	public void mostrarVentana()
	{
		ContactoDAOSQL contacto= new ContactoDAOSQL();
		ArrayList<String> list=(ArrayList<String>) contacto.leer();
		list.add("Conocido");
		comboTipoContacto.setModel(new DefaultComboBoxModel(list.toArray()));
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}
	
	public JComboBox getComboLocalidad() {
		return comboLocalidad;
	}

	public JComboBox getComboTipoContacto() {
		return comboTipoContacto;
	}

	public JButton getBtnEditarPersona() 
	{
		return btnEditarPersona;
	}

	public JCheckBox getChckbxFavoritos() {
		return chckbxFavoritos;
	}
	
	public int getIdPersona() {
		return idPersona;
	}
	
	public void setIdPersona(int id) {
		this.idPersona=id;
	}
	
	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.dispose();
	}
	
	public static void enable(JComboBox comboBox) {
        // has to be editable
        comboBox.setEditable(true);
        // change the editor's document
        new AutoCompletion(comboBox);
    }
}

