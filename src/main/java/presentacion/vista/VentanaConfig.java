package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JButton;

public class VentanaConfig extends JFrame 
{
	
	private JPanel contentPane;
	private static VentanaConfig INSTANCE;
	private JTextField txtUsuario;
	private JTextField txtPassw;
	private JTextField txtServer;
	private JButton btnGuardar;
	private JButton btnConfirmar;
	

	public static VentanaConfig getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaConfig(); 	
			return new VentanaConfig();
		}
		else
			return INSTANCE;
	}

	private VentanaConfig() 
	{
		super();
		setTitle("Configuracion");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 413, 205);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 377, 145);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblCredenciales = new JLabel("Credenciales de MySQL");
		lblCredenciales.setBounds(10, 11, 190, 14);
		panel.add(lblCredenciales);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 23, 357, 2);
		panel.add(separator);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setBounds(10, 64, 95, 14);
		panel.add(lblUsuario);
		
		JLabel lblPassw = new JLabel("Contrase\u00F1a:");
		lblPassw.setBounds(10, 89, 95, 14);
		panel.add(lblPassw);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(114, 61, 190, 20);
		panel.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		txtPassw = new JTextField();
		txtPassw.setColumns(10);
		txtPassw.setBounds(115, 86, 190, 20);
		panel.add(txtPassw);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(288, 111, 89, 23);
		panel.add(btnGuardar);
		
		JLabel lblServer = new JLabel("ServidorMySQL:");
		lblServer.setBounds(10, 36, 95, 14);
		panel.add(lblServer);
		
		txtServer = new JTextField();
		txtServer.setColumns(10);
		txtServer.setBounds(114, 36, 190, 20);
		panel.add(txtServer);
		
		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(10, 111, 89, 23);
		panel.add(btnConfirmar);
		btnConfirmar.setVisible(false);

		this.setVisible(false);
		
	}


	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public void cerrar()
	{
		this.dispose();
	}
	

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public void setTxtUsuario(JTextField txtUsuario) {
		this.txtUsuario = txtUsuario;
	}

	public JTextField getTxtPassw() {
		return txtPassw;
	}

	public void setTxtPassw(JTextField txtPassw) {
		this.txtPassw = txtPassw;
	}

	public JTextField getTxtServer() {
		return txtServer;
	}

	public void setTxtServer(JTextField txtServer) {
		this.txtServer = txtServer;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}
	
	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}
}

