package modelo;

import java.util.List;

import dto.ContactoDTO;
import dto.PersonaDTO;
import persistencia.dao.interfaz.ContactoDAO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.PersonaDAO;


public class Agenda 
{
	private PersonaDAO persona;	
	private ContactoDAO contacto;
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.contacto=metodo_persistencia.createContactoDAO();
	}
	
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}
	
	public void modificarPersona(PersonaDTO persona_a_modificar) {
		this.persona.update(persona_a_modificar);
	}
	
	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();		
	}
	
	public List<ContactoDTO> obtenerContactos()
	{
		return this.contacto.readAll();		
	}
	
	public void agregarContacto(ContactoDTO nuevoContacto)
	{
		this.contacto.insert(nuevoContacto);
	}

	public void borrarContacto(ContactoDTO contacto_a_eliminar) 
	{
		this.contacto.delete(contacto_a_eliminar);
	}
	
	public void modificarContacto(ContactoDTO contacto_a_modificar) {
		this.contacto.update(contacto_a_modificar);
	}
	
}
