CREATE DATABASE `grupo_6`;
USE grupo_6;
CREATE TABLE `personas` (
  `idPersona` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  `Calle` varchar(45) DEFAULT NULL,
  `Altura` varchar(6) DEFAULT NULL,
  `Piso` varchar(3) DEFAULT NULL,
  `Dpto` varchar(6) DEFAULT NULL,
  `Localidad` varchar(64) DEFAULT NULL,
  `Email` varchar(64) DEFAULT NULL,
  `Fecha_cumple` date DEFAULT NULL,
  `Tipo_contacto` varchar(45) DEFAULT NULL,
  `Favorito` tinyint NOT NULL DEFAULT '0' COMMENT 'Contacto Favorito es 1, para no favoritos es 0',
  PRIMARY KEY (`idPersona`)
) ;
CREATE TABLE `contactos` (
  `idContacto` int NOT NULL AUTO_INCREMENT,
  `tipoContacto` varchar(45) NOT NULL,
 PRIMARY KEY (`idContacto`)
) ;